while True:
    huruf_vokal = ['a','i','u','e','o']
    nama = str(input("Ketik Nama : "))
    total = 0
    data = ""
    for huruf in nama:
        if huruf in huruf_vokal and huruf not in data:
            total += 1
            data += huruf + " "
    print('"{}" = {} yaitu {} dan {}'.format(nama, total, data.split(" ")[0], data.split(" ")[1]))