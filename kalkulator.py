class Stack:
     def __init__(self):
         self.items = []
         
     def is_empty(self):
         return self.items == []

     def push(self, item):
         self.items.append(item)

     def pop(self):
         return self.items.pop()

     def peek(self):
         return self.items[len(self.items)-1]

     def size(self):
         return len(self.items)
         
def prior(former, latter):
    if ((former.__eq__("*") or former.__eq__("/")) and (latter.__eq__("+") or latter.__eq__("-") or latter.__eq__("("))):
        return True
    elif ((former.__eq__("+") or former.__eq__("-")) and latter.__eq__("(")):
        return True
    else:
        return False

def figureout(former, operator, latter):
    if operator.__eq__("+"):
        return former + latter
    elif operator.__eq__("-"):
        return former - latter
    elif operator.__eq__("*"):
        return former * latter
    elif operator.__eq__("/"):
        return former / latter
    else:
        return 0

def classifyOps(operator):
    if operator.__eq__("0") + operator.__eq__("1") + operator.__eq__("2") + operator.__eq__("3") + operator.__eq__("4") + operator.__eq__("5") + operator.__eq__("6") + operator.__eq__("7") + operator.__eq__("8") + operator.__eq__("9") + operator.__eq__("."):
        return 0
    elif operator.__eq__("+") + operator.__eq__("-") + operator.__eq__("*") + operator.__eq__("/"):
        return 1
    elif operator.__eq__("("):
        return 2
    elif operator.__eq__(")"):
        return 3
    else:
        return 4

def Calculator(strParam):
    stkNumber = Stack()
    stkOps = Stack()
    number = ""
    strParam = "(" + strParam + ")"
    i = 0
    while (i < len(strParam)):
        if classifyOps("" + strParam[i]) == 0:
            number = number + strParam[i]
        else:
            if number != "":
                stkNumber.push(int(number))
            number = ""
            if classifyOps("" + strParam[i]) == 2:
                stkOps.push("" + strParam[i])
            if classifyOps("" + strParam[i]) == 3:
                while(stkOps.peek() != "("):
                    latter = int(stkNumber.pop())
                    former = int(stkNumber.pop())
                    stkNumber.push(figureout(former, stkOps.pop(), latter))
                stkOps.pop()
            if classifyOps("" + strParam[i]) == 1:
                if prior(("" + strParam[i]), stkOps.peek()):
                    stkOps.push("" + strParam[i])
                else:
                    latter = int(stkNumber.pop())
                    former = int(stkNumber.pop())
                    while(prior(("" + strParam[i]), stkOps.peek()) != True):
                        stkNumber.push(figureout(former,stkOps.pop(),latter))
                    stkOps.push("" + strParam[i])
        i += 1
    while(stkOps.is_empty() != True):
        latter = int(stkNumber.pop())
        former = int(stkNumber.pop())
        stkNumber.push(figureout(former,stkOps.pop(), latter))
    return ("%d" % stkNumber.pop())
while True:
    s = input()
    print(Calculator(s))